package performanceactive.androidlayoutgenerator;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class DimensionUtils {
	
	protected final String dp = "dp";
	protected final String sp = "sp";
	
	protected static final String OUTPUT_FOLDER = "output";
	protected static final String LAYOUT_FOLDER = "layout";
	protected static final String REG_EX = "(\\d+(.\\d+)?)(dp|sp)";

	protected static final String RESOURCES_TAG = "resources";
	protected static final String DIMEN_TAG = "dimen";
	protected static final String NAME_ATTRIBUTE = "name";

	protected static final String DIMENS_PREFIX = "dimens_";


	public DimensionUtils() {
		super();
	}

	protected double round(double value, int places) {
	
		if (places < 0) {
			throw new IllegalArgumentException();
		}
	
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
	
		return bd.doubleValue();
	}

	protected HashMap<String, Double> explodeDoubleAndUnit(String originalVal) {
	
		HashMap<String, Double> returnable = new HashMap<>();
		String valType = "";
		Double value = 0.0;
	
		if (originalVal.contains(dp)) {
			valType = dp;
			value = Double.parseDouble(originalVal.replace(dp, ""));
		} else if (originalVal.contains(sp)) {
			valType = sp;
			value = Double.parseDouble(originalVal.replace(sp, ""));
		}
	
		returnable.put(valType, value);
		return returnable;
	}

	protected void outputDocumentToFile(Transformer tf, final String outputFolder, final String valuesFolder, final String fileName, Document document)
			throws TransformerException {
			
				final String folder = outputFolder + File.separator + valuesFolder;
				File fileDirectory = new File(folder);
				fileDirectory.mkdir();
			
				StreamResult result = new StreamResult(new File(folder.concat(File.separator).concat(fileName)));
			
				tf.transform(new DOMSource(document), result);
			
				System.out.println(valuesFolder.concat(File.separator).concat(fileName).concat(" File Saved!"));
				if (valuesFolder.equals(LAYOUT_FOLDER)) {
					System.out.println();
				}
			}

}