/**
 * 
 */
package performanceactive.androidlayoutgenerator;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DimensionGenerator extends DimensionUtils {


	private static final String valuessw320dp = "values-sw320dp";
	private static final String valuessw360dp = "values-sw360dp";
	private static final String valuessw480dp = "values-sw480dp";
	private static final String valuessw600dp = "values-sw600dp";
	private static final String valuessw720dp = "values-sw720dp";

	private String layoutFileName = "layout-file.xml";

	private int devSmallestWidth;
	
	private String idString = "";
	private Document original380doc;
	private Document document320;
	private Document document360;
	private Document document480;
	private Document document600;
	private Document document720;

	public DimensionGenerator(File file, int devSmallestWidth) {

		layoutFileName = file.getName();
		this.devSmallestWidth = devSmallestWidth;

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();

			createDocuments(documentBuilder, file);

			recursiveXMLCycle(original380doc);

			outputDocumentsToFile();

		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private void recursiveXMLCycle(Node node) {

		NodeList list = node.getChildNodes();

		for (int i = 0; i < list.getLength(); i++) {

			Node childNode = list.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {

				NamedNodeMap attributes = childNode.getAttributes();

				cycleNodeAttributes(attributes, childNode);

				recursiveXMLCycle(childNode);
			}
		}
	}

	private void cycleNodeAttributes(NamedNodeMap attributes, Node childNode) {

		for (int j = 0; j < attributes.getLength(); j++) {

			Node attribute = attributes.item(j);

			if (attribute.getNodeType() == Node.ATTRIBUTE_NODE) {

				performAttributeNodeOperation(attribute);
			}
		}
	}

	private void performAttributeNodeOperation(Node attribute) {

		String attributeType = getAttributeType(attribute);
		String attributeContent = attribute.getTextContent();

		if (attributeType.equals("Id")) {
			idString = removeIDPrefix(attributeContent);
		}

		final String dimensionName = idString + attributeType;

		final Pattern pattern = Pattern.compile(REG_EX, Pattern.CASE_INSENSITIVE);
		final Matcher matcher = pattern.matcher(attributeContent);
		if (matcher.find()) {

			attribute.setTextContent("@dimen/".concat(dimensionName));
			final String dimension = matcher.group();

			addElementToDocumentWithKeyOrigVal(document320, dimensionName, dimension);
			addElementToDocumentWithKeyOrigVal(document360, dimensionName, dimension);
			addElementToDocumentWithKeyOrigVal(document480, dimensionName, dimension);
			addElementToDocumentWithKeyOrigVal(document600, dimensionName, dimension);
			addElementToDocumentWithKeyOrigVal(document720, dimensionName, dimension);
		}
	}

	private String getAttributeType(Node attribute) {

		String attributeType = "";
		String[] attributeTypeArray = attribute.toString().split(":");
		if (attributeTypeArray.length > 1) {

			String cutoffAndroid = attributeTypeArray[1];
			String[] attributeTypeArrayNoValue = cutoffAndroid.split("=");

			if (attributeTypeArrayNoValue.length > 1) {

				attributeType = attributeTypeArrayNoValue[0];
			}
		}

		if (!attributeType.equals("")) {

			if (attributeType.contains("_")) {

				String[] removeUnderScoreArray = attributeType.split("_");
				attributeType = "";
				for (int i = 0; i < removeUnderScoreArray.length; i++) {

					attributeType += removeUnderScoreArray[i].substring(0, 1).toUpperCase(Locale.UK)
							+ removeUnderScoreArray[i].substring(1);
				}
			} else {

				attributeType = attributeType.substring(0, 1).toUpperCase(Locale.UK) + attributeType.substring(1);
			}

			if (attributeType.contains("Layout")) {

				attributeType = attributeType.replace("Layout", "");
			}
		}

		return attributeType.trim();
	}

	private String removeIDPrefix(String idValue) {

		final String plusId = "@+id/";
		final String noPlusId = "@id/";

		if (idValue.contains(plusId)) {
			idValue = idValue.replace(plusId, "");
		} else if (idValue.contains(noPlusId)) {
			idValue = idValue.replace(noPlusId, "");
		}

		return idValue;
	}

	private void createDocuments(DocumentBuilder documentBuilder, File file) throws Exception {

		original380doc = documentBuilder.parse(new FileInputStream(file));

		document320 = documentBuilder.newDocument();
		Element resourcesRootElement = document320.createElement(RESOURCES_TAG);
		document320.appendChild(resourcesRootElement);

		document360 = documentBuilder.newDocument();
		Element resourcesRootElement360 = document360.createElement(RESOURCES_TAG);
		document360.appendChild(resourcesRootElement360);

		document480 = documentBuilder.newDocument();
		Element resourcesRootElement480 = document480.createElement(RESOURCES_TAG);
		document480.appendChild(resourcesRootElement480);

		document600 = documentBuilder.newDocument();
		Element resourcesRootElement600 = document600.createElement(RESOURCES_TAG);
		document600.appendChild(resourcesRootElement600);

		document720 = documentBuilder.newDocument();
		Element resourcesRootElement720 = document720.createElement(RESOURCES_TAG);
		document720.appendChild(resourcesRootElement720);
	}

	private Double determineValueBasedOnDocument(Document document, Double number) {

		if (document.equals(document320)) {
			number = (number / devSmallestWidth) * AndroidLayoutGenerator.SW_320_DP;
		} else if (document.equals(document360)) {
			number = (number / devSmallestWidth) * AndroidLayoutGenerator.SW_360_DP;
		} else if (document.equals(document480)) {
			number = (number / devSmallestWidth) * AndroidLayoutGenerator.SW_480_DP;
		} else if (document.equals(document600)) {
			number = (number / devSmallestWidth) * AndroidLayoutGenerator.SW_600_DP;
		} else if (document.equals(document720)) {
			number = (number / devSmallestWidth) * AndroidLayoutGenerator.SW_720_DP;
		}

		return round(number, 2);
	}

	private void addElementToDocumentWithKeyOrigVal(Document document, String key, String originalVal) {

		HashMap<String, Double> exploded = explodeDoubleAndUnit(originalVal);

		Map.Entry<String, Double> entry = exploded.entrySet().iterator().next();
		Double number = entry.getValue();

		number = determineValueBasedOnDocument(document, number);

		String unit = entry.getKey();

		Element dimensionsElement = document.createElement(DIMEN_TAG);
		dimensionsElement.setAttribute(NAME_ATTRIBUTE, key);
		dimensionsElement.insertBefore(document.createTextNode(String.valueOf(number).concat(unit)),
				dimensionsElement.getLastChild());

		Node root = document.getChildNodes().item(0);
		root.appendChild(dimensionsElement);

	}

	private void outputDocumentsToFile() throws Exception {

		Transformer tf = TransformerFactory.newInstance().newTransformer();
		tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		tf.setOutputProperty(OutputKeys.INDENT, "yes");

		final String outputFolder = System.getProperty("user.dir") + File.separator + OUTPUT_FOLDER;

		File outputDirectory = new File(outputFolder);
		outputDirectory.mkdir();

		final String dimensFileName = DIMENS_PREFIX.concat(layoutFileName);

		outputDocumentToFile(tf, outputFolder, valuessw320dp, dimensFileName, document320);
		outputDocumentToFile(tf, outputFolder, valuessw360dp, dimensFileName, document360);
		outputDocumentToFile(tf, outputFolder, valuessw480dp, dimensFileName, document480);
		outputDocumentToFile(tf, outputFolder, valuessw600dp, dimensFileName, document600);
		outputDocumentToFile(tf, outputFolder, valuessw720dp, dimensFileName, document720);

		outputDocumentToFile(tf, outputFolder, LAYOUT_FOLDER, layoutFileName, original380doc);
	}

}
