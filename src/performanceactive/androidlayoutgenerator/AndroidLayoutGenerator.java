package performanceactive.androidlayoutgenerator;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AndroidLayoutGenerator {

	private static final String ALLOWED_FILE_TYPE = "xml";

	public static final int SW_320_DP = 320;
	public static final int SW_360_DP = 360;
	public static final int SW_480_DP = 480;
	public static final int SW_600_DP = 600;
	public static final int SW_720_DP = 720;

	public static void main(String[] args) {
		
		System.out.println("getting here");

		int devSmallestWidth = SW_360_DP;

		if (args.length >= 1 && args.length <= 2) {

			File file = new File(args[0]);

			if (args.length == 2) {

				String devSmallestWidthString = args[1];

				final Integer[] allowedInputs = { SW_320_DP, SW_360_DP, SW_480_DP, SW_600_DP, SW_720_DP };

				List<Integer> allowedInputsList = new ArrayList<Integer>(Arrays.asList(allowedInputs));

				try {

					devSmallestWidth = Integer.parseInt(devSmallestWidthString);

					if (!allowedInputsList.contains(new Integer(devSmallestWidth))) {

						throw new NumberFormatException();
					}

				} catch (NumberFormatException nfe) {

					System.err.println("Smallest width is not an integer." + "\nPlease enter either : "
							+ allowedInputsList + "" + "\nThe Smallest Width category you developed the layouts on.");
					return;
				}
			}

			if (file.exists()) {

				if (file.isFile()) {

					executeAndroidLayoutGenerator(file, devSmallestWidth);

				} else if (file.isDirectory()) {

					File[] files = file.listFiles();
					recursiveFilesCycle(files, devSmallestWidth);
				}
			}

		} else {

			System.err
					.println("Invalid parameter length. " + "Please give the Root folder or a single XML Layout File");
		}
	}

	private static void executeAndroidLayoutGenerator(File file, int devSmallestWidth) {
		if (getFileType(file.getName()).equals(ALLOWED_FILE_TYPE)) {
			new DimensionGenerator(file, devSmallestWidth);
		}
	}

	private static void recursiveFilesCycle(File[] files, int devSmallestWidth) {

		for (File file : files) {
			if (file.isDirectory()) {
				recursiveFilesCycle(file.listFiles(), devSmallestWidth);
			} else {
				executeAndroidLayoutGenerator(file, devSmallestWidth);
			}
		}
	}

	private static String getFileType(String fileName) {
		String extension = "";

		int i = fileName.lastIndexOf('.');
		int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

		if (i > p) {
			extension = fileName.substring(i + 1);
		}
		return extension;
	}

}
